
public class Skill 
{
	private String name;
	private int yearsOfUsage;
	
	
	public Skill() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Skill(String name, int yearsOfUsage) {
		super();
		this.name = name;
		this.yearsOfUsage = yearsOfUsage;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getYearsOfUsage() {
		return yearsOfUsage;
	}


	public void setYearsOfUsage(int yearsOfUsage) {
		this.yearsOfUsage = yearsOfUsage;
	}

	
	
	
}
